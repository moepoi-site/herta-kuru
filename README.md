# Herta Kuru ~

<p align="center">
    <a href="https://herta.moe.team">Click Here to Squish the kuru~!</a>
    <br>
    <a href="http://t.me/Herta_Kuru_Bot/kuru">(Telegram Version)</a>
</p>

![](https://herta.moe.team/img/herta1.gif)

## The website for Herta, the cutest genius Honkai: Star Rail character out there!

<br>

Herta GIF and Art made by [@Seseren_kr](https://twitter.com/Seseren_kr)
//varible
const audioList = [
    new Audio("audio/kuruto.mp3"),
    new Audio("audio/kuru1.mp3"),
    new Audio("audio/kuru2.mp3"),
];

for (const audio of audioList) {
    audio.preload = "auto";
}

let firstSquish = true;
//end varible

if (!localStorage.getItem("count")) {
    localStorage.setItem("count", 0);
}

let temporaryCounter = parseInt(localStorage.getItem("count"));
const counterElement = document.getElementById("counter");
const counterButton = document.getElementById("counter-button");

displayCounter(temporaryCounter);

//counter button
async function counterClick() {
    ++temporaryCounter;
    displayCounter(temporaryCounter);
    Telegram.WebApp.HapticFeedback.impactOccurred('medium');

    playKuru();
    animateHerta();

    counterButton.addEventListener("click", triggerRipple);

    localStorage.setItem("count", temporaryCounter);

    if (temporaryCounter % 100 === 0) {
        Telegram.WebApp.showPopup({"title": "Congratulations 🎉", "message": `You have squished the kuru~ ${temporaryCounter} times! 🫡`, "buttons": [{"type": "destructive", "text": "OK, kuru !"}]});
    }
}

function displayCounter(value) {
    counterElement.innerText = value;
}

function playKuru() {
    let audio;

    if (firstSquish) {
        firstSquish = false;
        audio = audioList[0].cloneNode();
    } else {
        const random = Math.floor(Math.random() * 2) + 1;
        audio = audioList[random].cloneNode();
    }

    audio.play();

    audio.addEventListener("ended", function () {
        this.remove();
    });
}

function animateHerta() {
    let id = null;

    const random = Math.floor(Math.random() * 2) + 1;
    const elem = document.createElement("img");
    elem.src = `img/herta${random}.gif`;
    elem.style.position = "absolute";
    elem.style.right = "-500px";
    elem.style.top = counterButton.getClientRects()[0].bottom + scrollY - 430 + "px"
    elem.style.zIndex = "-1";
    document.body.appendChild(elem);

    let pos = -500;
    const limit = window.innerWidth + 500;
    clearInterval(id);
    id = setInterval(() => {
        if (pos >= limit) {
            clearInterval(id);
            elem.remove()
        } else {
            pos += 20;
            elem.style.right = pos + 'px';
        }
    }, 12);
}

function triggerRipple(e) {
    let ripple = document.createElement("span");
        
    ripple.classList.add("ripple");

    this.appendChild(ripple);

    let x = e.clientX - e.target.offsetLeft;

    let y = e.clientY - e.target.offsetTop;

    ripple.style.left = `${x}px`;
    ripple.style.top = `${y}px`;

    setTimeout(() => {
        ripple.remove();
    }, 300);
}
//end counter button

function openURL(url) {
    Telegram.WebApp.openLink(url);
}